# Arduino Processing - Dibujar con Joystick

Se presentan dos archivos.

Uno corresponde al código que ejecuta la placa Arduino, utilizando un joystick y leyendo en los pines A4 y A5.

Por el otro lado, el código de Processing, basado en el ejemplo SimpleRead para leer en el puerto serie.

Se recuperan los datos para las coordenadas x e y y cada vez que la caja toca un borde, cambia el color de su trazo.
