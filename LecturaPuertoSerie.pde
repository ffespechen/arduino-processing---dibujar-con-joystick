/**
 * Simple Read
 * 
 * Read data from the serial port and change the color of a rectangle
 * when a switch connected to a Wiring or Arduino board is pressed and released.
 * This example works with the Wiring / Arduino program that follows below.
 */


import processing.serial.*;

Serial myPort;  // Create object from Serial class
float[] val;      // Data received from the serial port
float x = 0;
float y = 0;
float anchoCaja = 15;
float altoCaja = 15;

int incrementoX = 5;
int incrementoY = 5;

void setup()
{
  size(700, 400);
  println(Serial.list());//ver e que puerto esta el arduino
  myPort = new Serial(this, "/dev/ttyUSB0", 9600);//crea e puerto 9600 debe ser igaul en Arduino
  myPort.bufferUntil('\n');

  background(0);
  fill(255);
  
  x = width/2;
  y = height/2;
  
}

void draw()
{
  
  //background(0);
  rect(x, y, anchoCaja, altoCaja);
  
  if(x>width)
  {
    x=width-anchoCaja;
    cambiarColor();
  }
  
  if(x<0)
  {
    x=0;
    cambiarColor();
  }
  
  if(y>height)
  {
    y=height-altoCaja;
    cambiarColor();
  }
  
  if(y<0)
  {
    y=0;
    cambiarColor();
  }
  
  
  if(val!=null)
  {
    if(val[0]>600)
    {
      x = x+incrementoX;
    }
    else if(val[0]<400)
    {
      x = x-incrementoX;
    }
    
    if(val[1]>600)
    {
      y = y-incrementoY;
    }
    else if(val[1]<400)
    {
      y = y+incrementoY;
    }
    
  }
  

}

void cambiarColor()
{
  fill(random(255), random(255), random(255));
}

void serialEvent(Serial port) {
  String bufString = port.readString();
  val = float(split(bufString, ','));
  
  for(int j=0; j< val.length; j++)
  {
    print("valor "+j+" : "+str(val[j])+"\t");
    print("\n");
  }
    
}
